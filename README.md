## My Jupyter Notebooks

[Building a Classifer using Wikipedia, Python3, and NLTK](http://nbviewer.jupyter.org/urls/bitbucket.org/rberkes/notebooks/raw/377fa3af1a928b2a1bbfe2317916253a982eded1/Drafts/Building%20A%20Wikipedia%20Classifier%20with%20Python3%20and%20NLTK.ipynb)

[Ramanujan's Apartment Puzzle](http://nbviewer.jupyter.org/urls/bitbucket.org/rberkes/notebooks/raw/377fa3af1a928b2a1bbfe2317916253a982eded1/Drafts/Ramanujan%20Puzzle.ipynb)

[Creating Convincing Tweets with Markov Chains](http://nbviewer.jupyter.org/urls/bitbucket.org/rberkes/notebooks/raw/59c36f9beada37710fd059335b4e52949fdfd722/Drafts/DOC_Twitter%20Chatbot%20Using%20Markov%20Chains%20and%20Corpus.ipynb)

[Predicting Football Games with a Neural Net in PyBrain](http://nbviewer.jupyter.org/urls/bitbucket.org/rberkes/notebooks/raw/da38182899e142cdb61e367c6a59ae60677cd9ce/Drafts/HandEgg%20PyBrain%202015.ipynb)