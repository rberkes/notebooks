import tensorflow as tf 

#filename_queue = tf.train.string_input_producer(["scores_stocks.log",])

reader = tf.TextLineReader("scores_stocks.log")
key, value = reader.read(filename_queue)

record_defaults = [[1], [1], [1], [1], [1]]

col1, col2, col3, col4, col5 = tf.decode_csv(
    value, record_defaults=record_defaults)
features = tf.pack([col1,col2,col4,col5])

with tf.Session("gprc://127.0.0.1:2222") as sess:
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)

    for i in range(1000):
        example, label = sess.run([features,col3])
    coord.request_stop()
    coord.join(threads)
