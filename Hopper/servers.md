##Slingo Servers External Traffic

###Icarus--
------
MySQL Bound to *:3306 but Netstat only shows connections to local 192.168.1 addresses, as below, no public external addresses bound. Only other publicly bound is rhn_check daemon for running checks to the Red Hat Network for system updates. 

[root@icarus realnets]# netstat -anp
Active Internet connections (servers and established)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name          
tcp        0      0 0.0.0.0:3306            0.0.0.0:*               LISTEN      16012/                            
tcp        0      0 192.168.1.30:3306       192.168.1.205:43809     ESTABLISHED 16012/                             
tcp        0      0 192.168.1.30:3306       192.168.1.205:46210     ESTABLISHED 16012/              
tcp        0      0 192.168.1.30:3306       192.168.1.205:45811     ESTABLISHED 16012/              

root     15986     1  0  2014 ?        00:00:00 /bin/sh /usr/bin/mysqld_safe --datadir=/usr/local/mysql/data --pid-file=/usr/local/mysql/data/icarus.slingo.com.pid
mysql    16012 15986  0  2014 ?        19:11:19 [mysqld-max]

###Hercules02  
MySQL publicly bound, netstat shows no active conns.  Only active connection shown through netstat is my SSH connection. 

###Heaven2/Web115 
Down? / Cannot ssh or ping IP address

###cron099
Cannot ssh or ping IP

###www10 
is serving SVN pages for slingo,  last good hits on 3/24.  MySQL also publicly bound. No logs for MySQL, however, no updates to files, including ibdata, which gets normally updated with every transaction for innodb tables, since Oct of 2012.  Log for Apache:
            217.24.20.62 - goran  [24/Mar/2015:05:50:10 -0500] "GET /svn/slingo/Technology/SGS/client-js-new/com/slingo/flash/sgs/wf9/SgsWrapper.as HTTP/1.1" 200 55890 "http://subversion.slingo.com/svn/slingo/Technology/SGS/client-js-new/com/slingo/flash/sgs/wf9/" "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36"

###web116
found IP 66.246.127.86 for web116 but cannot ping or ssh successfully

###postoffice02
dnscache process is bound publicly, established connection to public ip, many connections to one machine, as shown below,

tcp        0      0 :::22                       :::*                        LISTEN      1759/sshd           
tcp        0      0 ::ffff:66.246.127.41:22     ::ffff:207.188.17.52:62470  ESTABLISHED 20570/sshd: realnet 
tcp        0      0 ::ffff:66.246.127.41:22     ::ffff:59.45.79.116:34542   ESTABLISHED 20644/sshd: root [p 
udp        0      0 0.0.0.0:32768               0.0.0.0:*                               1435/rpc.statd      
udp        0      0 66.246.127.41:24705         173.245.58.194:53           ESTABLISHED 2000/dnscache       
udp        0      0 66.246.127.41:39298         173.245.59.164:53           ESTABLISHED 2000/dnscache       
udp        0      0 66.246.127.41:47618         173.245.58.194:53           ESTABLISHED 2000/dnscache       
udp        0      0 66.246.127.41:7938          173.245.59.164:53           ESTABLISHED 2000/dnscache       
udp        0      0 66.246.127.41:60548         173.245.58.194:53           ESTABLISHED 2000/dnscache       
udp        0      0 66.246.127.41:26373         173.245.58.194:53           ESTABLISHED 2000/dnscache       
udp        0      0 66.246.127.41:7429          173.245.59.164:53           ESTABLISHED 2000/dnscache       
udp        0      0 66.246.127.41:33285         173.245.58.194:53           ESTABLISHED 2000/dnscache       

machine dig -x :
[root@postoffice02 realnets]# dig -x 173.245.59.164

; <<>> DiG 9.3.1 <<>> -x 173.245.59.164
;; global options:  printcmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 6668
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;164.59.245.173.in-addr.arpa.	IN	PTR

;; ANSWER SECTION:
164.59.245.173.in-addr.arpa. 1800 IN	PTR	ernest.ns.cloudflare.com.

;; Query time: 98 msec
;; SERVER: 192.168.1.41#53(192.168.1.41)
;; WHEN: Fri Mar 27 13:00:30 2015
;; MSG SIZE  rcvd: 83

[root@postoffice02 realnets]# 

###postoffice01
tcpserver and dnscache DNS processes are running, but not bound to any IP addresses.  No log entries are found on system.  Only bound connections are to portmap process shown below:

tcp        0      0 64.21.82.154:111            61.240.144.65:42752         ESTABLISHED 1763/portmap        
tcp        0      0 64.21.82.154:111            61.240.144.67:60000         ESTABLISHED 1763/portmap  

###horde

uptime
 14:48:04 up 1229 days,  1:53,  1 user,  load average: 0.01, 0.01, 0.00
 
 serving horde.slingo.com and other subnets
         
###apollo2
cannot ssh or ping

###apollo
BSD, login as root
FTP server, very quiet, some changed files as recent as Oct 2014 though in /usr/home/*  and /home/mysql-ftp for ftp roots

###logs01/pdsvn
svn under /usr/local/svn , ftp files under /home/,  vsftpd daemon.  both active according to file mod times ...

###loadme01
neither realnets nor root logins work. can ping.

###exchange02
administrator, Admin,admin,Administrator, realnets account do not work with provided pw
 
