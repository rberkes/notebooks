from elasticsearch import Elasticsearch
import json
es = Elasticsearch()
ifp = open('/home/rob/Downloads/enwiki-latest-all-titles-in-ns0','r')
LINENUM=1
for line in ifp:
    line = line.strip()
    url = 'https://en.wikipedia.org/wiki/'+str(line)
    jsonobject = json.dumps({'id':LINENUM,
                             'title': str(line),
                             'url': url},indent=3,sort_keys=True)
    result = es.index(index="enwiki",doc_type="link",body=jsonobject,id=LINENUM)
    LINENUM+=1
    if LINENUM % 1000 == 0 :
        print(str(LINENUM/1000)+" thousand records inserted!")

