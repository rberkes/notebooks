NUM_TEAMS=32
ROUND_TO=3

class TeamRanks():
    off_rush_rank=0
    off_rush_rel_rank=0.0
    off_pass_rank=0
    off_pass_rel_rank=0.0
    def_rush_rank=0
    def_rush_rel_rank=0.0
    def_pass_rank=0
    def_pass_rel_rank=0.0
    off_yards_per=0
    off_yards_per_rank=0.0
    off_turn_ovs=0
    off_turn_ovs_rank=0.0
    def_points_per=0
    def_points_per_rank=0.0
    def_yards_per=0
    def_yards_per_rank=0.0
    
    def __init__(self,name):
        self.name=name
    def update_stats(self):
        self.off_rush_rel_rank=round(self.off_rush_rank/NUM_TEAMS,ROUND_TO)
        self.off_pass_rel_rank=round(self.off_pass_rank/NUM_TEAMS,ROUND_TO)
        self.off_yards_per_rank=round(self.off_yards_per/NUM_TEAMS,ROUND_TO)
        self.off_turn_ovs_rank=round(self.off_turn_ovs/NUM_TEAMS,ROUND_TO)
        self.def_yards_per_rank=round(self.def_yards_per/NUM_TEAMS,ROUND_TO)
        self.def_points_per_rank=round(self.def_points_per/NUM_TEAMS,ROUND_TO)
        self.def_rush_rel_rank=round(self.def_rush_rank/NUM_TEAMS,ROUND_TO)
        self.def_pass_rel_rank=round(self.def_pass_rank/NUM_TEAMS,ROUND_TO)
    def stat_rankings(self):
        return self.off_rush_rel_rank,self.off_pass_rel_rank,self.def_rush_rel_rank,self.def_pass_rel_rank, \
        self.off_yards_per_rank,self.off_turn_ovs_rank,self.def_yards_per_rank,self.def_points_per_rank
import requests,json
KIMONO_API_KEY="VVBS8xfu2F8kOYpMIsl3UyDZJhjfLIAi"
NFLORYPG = '7hm4jen4'
KIMONOURL="https://www.kimonolabs.com/api/csv/%s?apikey=%s" % (NFLORYPG,KIMONO_API_KEY)
TEAMLIST=[]
RESULT = requests.get(KIMONOURL)
WORDLIST=['"index"','index',]
for row in RESULT.text.split('\n'):
    row=row.strip().split(",")
    try:
        #print(row[1],row[3])
        if "index" and '\"index\"' not in row[3] or not row[3]:
            TEAMLIST.append(TeamRanks(row[1]))
            rank=row[3].strip('"')
            rank=int(rank)
            TEAMLIST[-1].off_rush_rank=rank
    except IndexError:
        continue
        
KIMONO_API_KEY = "VVBS8xfu2F8kOYpMIsl3UyDZJhjfLIAi"
APIS={'NFLDRYPG': 'b6x3g290',
      'NFLDPYPG': '22c7y6do',
      'NFLOPYPG': 'b7f1obbk',
      #'NFLOYPG': 'cofor5ia',
      #'NFLOTO': '97y02wwo',
      'NFLDPPG':'dyobsda2',
      'NFLDYPG':'btrch90u',
     }
API2RANKS={'NFLDRYPG':"drr",
           'NFLDPYPG':"dpr",
           'NFLOPYPG':"opr",
           'NFLOYPG':"oypg",
           'NFLOTO':"oto",
           'NFLDPPG':"dppg",
           'NFLDYPG':"dypg"  
          }
NEWAPIS={'NFLOTO':'97y02wwo',
         'NFLOYPG': 'cofor5ia'}

for key,val in APIS.items():
    KIMONOURL="https://www.kimonolabs.com/api/csv/%s?apikey=%s" % (val,KIMONO_API_KEY)
    RESULT = requests.get(KIMONOURL)
    RESULT = RESULT.text
    for row in RESULT.split('\n'):
        row=row.strip().split(",")
        try:
            if row==['"collection1"'] or row[3]=='"index"':
                continue
        except IndexError:
            continue
        rank=row[3].strip('"')
        rank=int(rank)
        for team in TEAMLIST:
            if team.name in row[1]:
                if "drr" in API2RANKS[key]:                
                    team.def_rush_rank = rank
                elif "dpr" in API2RANKS[key]:
                    team.def_pass_rank = rank
                elif "opr" in API2RANKS[key]:
                    team.off_pass_rank = rank
                elif "dppg" in API2RANKS[key]:
                    team.def_points_per = rank
                elif "dypg" in API2RANKS[key]:
                    team.def_yards_per = rank
      

for key,val in NEWAPIS.items():
    KIMONOURL="https://www.kimonolabs.com/api/csv/%s?apikey=%s" % (val,KIMONO_API_KEY)
    RESULT = requests.get(KIMONOURL)
    RESULT = RESULT.text
    for row in RESULT.split('\n'):
        row=row.strip().split(",")
        try:
            if row==['"collection1"'] or row[2]=='"index"':
                continue
        except IndexError:
            continue        
        rank=row[2].strip('"')
        #print(row,rank)
        rank=int(rank)
        for team in TEAMLIST:
            if team.name in row[0]:
                if "oypg" in API2RANKS[key]:
                    team.off_yards_per = rank
                elif "oto" in API2RANKS[key]:
                    team.off_turn_ovs = rank
for team in TEAMLIST:
    print(team.name,team.off_pass_rank,team.off_rush_rank,team.def_rush_rank,team.def_pass_rank,team.off_yards_per,
          team.off_turn_ovs,team.def_points_per,team.def_yards_per)
#Build training dataset, from past results
from pybrain.datasets.classification import SequenceClassificationDataSet
KIMONOURL="https://www.kimonolabs.com/api/csv/%s?apikey=%s" % ('b6m5jj0s',KIMONO_API_KEY)
RESULT=requests.get(KIMONOURL)
RESULT=RESULT.text
DATASET=SequenceClassificationDataSet(16,2)
for team in TEAMLIST:
    team.update_stats()
for row in RESULT.split('\n'):
    row=row.strip().split(",")
    try:
        if row==['"collection1"'] or row[4]=='"index"':
            continue
    except IndexError:
        continue
    SCORES=[]
    SCORES=row[3].split(':')
    AWAY=row[1]
    AWAY=AWAY.strip('"')
    #AWAYSET=set(AWAY.strip(' '))
    for team in TEAMLIST:
        if AWAY in team.name or AWAY is team.name:
            STATLIST=team.stat_rankings()
    
    HOME=row[2]
    HOME=HOME.strip('"')
    HOME=HOME.strip("@")
    HOME=HOME.lstrip(' ')
    HOMESET=set(HOME.split(' '))
    for team in TEAMLIST:
        TEAMSET=set(team.name.split(' '))
        #print(HOME,team.name,HOME.capitalize == team.name,str(HOME) is team.name,HOMESET==TEAMSET)
        if HOME in team.name or AWAY is team.name:
            STATLIST+=team.stat_rankings()
        if team.name in HOME or team.name is HOME:
            STATLIST+=team.stat_rankings()
    try:
        AS=int(SCORES[0].strip('"'))
    except ValueError:
        print('ValueError found, skipping result, ',SCORES[0])
        continue
    try:
        HS=int(SCORES[1].strip('"'))
    except ValueError:
        print('ValueError found, skipping result, ',SCORES[1])
        continue
    if AS > HS:
        SCORESET=(1,0)
    elif HS > AS:
        SCORESET=(0,1)
    elif HS==AS:
        SCORESET=(0,0)
    try:
        DATASET.addSample(STATLIST,SCORESET)
        print(STATLIST,SCORESET)
    except:
        print('exception occurred, passing')
        pass
print(len(DATASET))

NNFILENAME='PyBrainNFL97Games.dill'
#
#LASTWEEK
#  format: {winner : loser ,
#           winner: loser,} 



THISWEEK={'Lions':'Chiefs',
          'Buccaneers':'Falcons',
          'Packers': 'Broncos',
          'Browns':'Cardinals',
          '49ers':'Rams',
          'Saints':'Giants',
          'Vikings':'Bears',
          'Chargers':'Ravens',
          'Bengals':'Steelers',
          'Titans':'Texans',
          'Jets':'Raiders',
          'Seahawks':'Cowboys',
          'Colts':'Panthers',
          'Dolphins':'Patriots'}

from pybrain.tools.neuralnets import NNclassifier
from pybrain.structure.connections import FullConnection
from pybrain.datasets import SupervisedDataSet
from pybrain.datasets.classification import SequenceClassificationDataSet
from pybrain.structure import FeedForwardNetwork,LinearLayer,SigmoidLayer,ReluLayer
import numpy as np
import dill 
#try to open neural network from file, if fails, init a new one
try:
    fp=open(NNFILENAME, 'rb')
    n = dill.load(fp)
    fp.close()
except:
    print("Loading Saved Neural Network Failed, creating from scratch.")
    #n = algorithms.Backpropagation(
    #   (4, 4, 1),
    #    step=0.1,
    #    verbose=False,
    #    show_epoch=5000,
    #    )
    n=FeedForwardNetwork()
    inLayer=LinearLayer(16,name='input_ranks')
    HiddenLayer=SigmoidLayer(9,name='hidden_calcs')
    outLayer=LinearLayer(2,name='output_odds')
    n.addInputModule(inLayer)
    n.addModule(HiddenLayer)
    n.addOutputModule(outLayer)
    in_to_hidden = FullConnection(inLayer, HiddenLayer)
    hidden_to_out = FullConnection(HiddenLayer, outLayer)
    n.addConnection(in_to_hidden)
    n.addConnection(hidden_to_out)
    n.sortModules()

    
from pybrain.structure import FullConnection
np.random.seed(1234)

from pybrain.supervised.trainers import BackpropTrainer

#classifier=NNclassifier(dset)
#graphics=classifier.initGraphics(ymax=1,xmax=1)

trainer=BackpropTrainer(n,DATASET)
for i in range(DATASET.getNumSequences()):
    for input_data, target in DATASET.getSequenceIterator(i):
        trds=SupervisedDataSet(16,2)
        trds.addSample(input_data,target)
        trainer.setData(trds)
        trainer.trainEpochs(10)
     #fewer epochs per result seems optimal 
#OFP.close()
#now that network is trained, save it for later loading ...
f=open(NNFILENAME,'wb')
dill.dump(n,f)
f.close()

#%matplotlib inline


#First, update the team stats
for team in TEAMLIST:
    team.update_stats()

for home, away in THISWEEK.items():
    NEWLIST=[]
    for team in TEAMLIST:
        if away in team.name:
            NEWLIST=team.stat_rankings()
    for team in TEAMLIST:
        if home in team.name:
            NEWLIST+=team.stat_rankings()
   # input_data = SequenceClassificationDataSet(8,2)
    input_data = np.array(
        NEWLIST
    )
    target_data = np.array([
        1,1
        ])
    ds=SupervisedDataSet(16,2)
    ds.addSample(input_data,target_data)
   # print(input_data,len(input_data))
    predicted=n.activateOnDataset(ds)
    #print(predicted[0][0])
    print("now predicting ",home,round(float(predicted[0][0]),3)," vs ",away,round(float(predicted[0][1]),3),end='*')
    print(" total difference: ",round(float(predicted[0][0]-predicted[0][1]),5),'*')
#print(type(trainer))  
net=n
for mod in net.modules:
    print("Module:", mod.name)
    if mod.paramdim > 0:
        print("--parameters:", mod.params)
    for conn in net.connections[mod]:
        print("-connection to", conn.outmod.name)
        if conn.paramdim > 0:
             print("- parameters", conn.params)
    if hasattr(net, "recurrentConns"):
        print("Recurrent connections")
        for conn in net.recurrentConns:
            print("-", conn.inmod.name, " to", conn.outmod.name)
            if conn.paramdim > 0:
                print("- parameters", conn.params)
