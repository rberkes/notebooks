import subprocess, os, glob,matplotlib,re
reAPR28=re.compile('/28/Apr/2015/')

for fname in glob.glob('SudokuLogs/*'):
    fp = open(fname,'r')
    for line in fp:
        line = line.strip().split()
        result = reAPR28.search(line[3])
        if result:
            print line
            break
